<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view ('halaman.register');
    }

    public function welcome(Request $request){
        $namadepan = $request['firstName'];
        $namabelakang = $request['lastName'];
        return view('halaman.welcome', compact('namadepan', 'namabelakang'));
    }
}
